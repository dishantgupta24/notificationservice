package com.cars24.notificationservice;

import com.cars24.notificationservice.exception.ValidationException;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    private static final String mobileNumberRegex = "^[7-9][0-9]{9}$";

    public static void isValidMobileNumber(String mobileNumber) throws ValidationException {
        if (!StringUtils.isNotEmpty(mobileNumber)){
            throw new ValidationException("Mobile Number is Empty");
        }
        if (!mobileNumber.matches(mobileNumberRegex)){
            throw new ValidationException(
                    String.format("Mobile number {%s} fails the regex {%s}", mobileNumber, mobileNumberRegex));
        }
    }

    public static void isNotEmptyString(String string) throws ValidationException {
        if (!StringUtils.isNotEmpty(string)){
            throw new ValidationException("Message is Empty");
        }
    }

  public static String getCurrentDateTimeString(){

      SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      return simpleDateFormat1.format(new Date());
  }
}
