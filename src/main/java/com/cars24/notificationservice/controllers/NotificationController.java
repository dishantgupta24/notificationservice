package com.cars24.notificationservice.controllers;

import com.cars24.notificationservice.constant.ApiConstant;
import com.cars24.notificationservice.dto.ApiResponse;
import com.cars24.notificationservice.dto.NotifyRequestPayload;
import com.cars24.notificationservice.exception.NotificationTypeException;
import com.cars24.notificationservice.exception.TemplateNotApproved;
import com.cars24.notificationservice.exception.TemplateNotFound;
import com.cars24.notificationservice.logging.LogMessage;
import com.cars24.notificationservice.services.NotificationService;
import com.cars24.notificationservice.services.NotificationServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;


@RestController
@RequestMapping("/cns/")
public class NotificationController {

    private static Logger logger = LoggerFactory.getLogger(NotificationController.class);

    @Autowired
    NotificationServiceProvider notificationServiceProvider;

    @PostMapping("/notify")
    public ResponseEntity<ApiResponse> notify(@Valid @RequestBody NotifyRequestPayload notifyRequestPayload) throws NotificationTypeException, TemplateNotFound, TemplateNotApproved {
        notifyRequestPayload.setRequestId(UUID.randomUUID().toString());
        logger.info(LogMessage.getRequestLogMessage(notifyRequestPayload, "Request Received"));
        NotificationService notificationService = notificationServiceProvider.get(
                notifyRequestPayload.getNotificationTypeEnum());
        notificationService.process(notifyRequestPayload);
        return ResponseEntity.ok().body(new ApiResponse(ApiConstant.SUCCESS_CODE, ApiConstant.SUCCESS_MESSAGE));
    }
}
