package com.cars24.notificationservice.controllers;

import com.cars24.notificationservice.constant.ApiConstant;
import com.cars24.notificationservice.dto.ApiResponse;
import com.cars24.notificationservice.exception.NotificationTypeException;
import com.cars24.notificationservice.exception.TemplateNotApproved;
import com.cars24.notificationservice.exception.TemplateNotFound;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlingControllerAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiResponse> handleException(MethodArgumentNotValidException e){
        return  ResponseEntity.badRequest().body(new ApiResponse(ApiConstant.BAD_RESPONSE_CODE, e.getMessage()));
    }

    @ExceptionHandler({HttpMessageNotReadableException.class, NotificationTypeException.class, TemplateNotFound.class})
    public ResponseEntity<ApiResponse> handleException(Exception e){
        return  ResponseEntity.badRequest().body(new ApiResponse(ApiConstant.BAD_RESPONSE_CODE, e.getMessage()));
    }

    @ExceptionHandler({TemplateNotApproved.class})
    public ResponseEntity<ApiResponse> handleTemplateNotApprovedException(Exception e){
        return  ResponseEntity.badRequest().body(new ApiResponse(ApiConstant.BAD_RESPONSE_CODE, "Template not approved . Please consult admin"));
    }

}
