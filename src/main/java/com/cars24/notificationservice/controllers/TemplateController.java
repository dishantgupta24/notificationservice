package com.cars24.notificationservice.controllers;

import com.cars24.notificationservice.Utils;
import com.cars24.notificationservice.constant.ApiConstant;
import com.cars24.notificationservice.dto.ApiResponse;
import com.cars24.notificationservice.dto.SmsTemplateApiResponse;
import com.cars24.notificationservice.entitiy.SmsTemplateEntity;
import com.cars24.notificationservice.services.template.SmsTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/cns/")
public class TemplateController {

    @Autowired
    SmsTemplateRepository smsTemplateRepository;

    @GetMapping("/template")
    public ResponseEntity<SmsTemplateApiResponse> get(
            @RequestParam(value = "templateName", required = false) String templateName){
        List<SmsTemplateEntity> smsTemplateEntityList = new ArrayList<>();
        if (templateName!=null) {
            SmsTemplateEntity smsTemplateEntity = smsTemplateRepository.findByTemplateName(templateName);
            smsTemplateEntityList.add(smsTemplateEntity);
        } else {
            smsTemplateEntityList = (List<SmsTemplateEntity>) smsTemplateRepository.findAll();
        }
        SmsTemplateApiResponse smsTemplateApiResponse = new SmsTemplateApiResponse(
                ApiConstant.SUCCESS_CODE,ApiConstant.SUCCESS_MESSAGE, smsTemplateEntityList);
        smsTemplateApiResponse.setSize(smsTemplateEntityList.size());
        return ResponseEntity.ok().body(smsTemplateApiResponse);

    }

    @PostMapping("/template")
    public ResponseEntity<ApiResponse> post(
            @Valid @RequestBody SmsTemplateEntity smsTemplateEntityPayload) {


        SmsTemplateEntity smsTemplateEntity = smsTemplateRepository.findByTemplateName(smsTemplateEntityPayload.getTemplateName());

        if( null == smsTemplateEntity){
            smsTemplateEntityPayload.setCreatedDate(Utils.getCurrentDateTimeString());
            smsTemplateEntityPayload.setUpdatedDate(Utils.getCurrentDateTimeString());
        }else {
            smsTemplateEntityPayload.setUpdatedDate(Utils.getCurrentDateTimeString());
        }

        smsTemplateRepository.save(smsTemplateEntityPayload);
        ApiResponse apiResponse = new ApiResponse(ApiConstant.SUCCESS_CODE,ApiConstant.SUCCESS_MESSAGE);
        return ResponseEntity.ok().body(apiResponse);
    }
}
