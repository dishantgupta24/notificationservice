package com.cars24.notificationservice.services;

import com.cars24.notificationservice.constant.NotificationTypeEnum;
import com.cars24.notificationservice.exception.NotificationTypeException;
import com.cars24.notificationservice.services.sms.SmsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceProvider {

    private static Logger logger = LoggerFactory.getLogger(NotificationServiceProvider.class);

    @Autowired
    SmsService smsService;

    public NotificationService get(NotificationTypeEnum notificationTypeEnum) throws NotificationTypeException {
        NotificationService service = null;
        if (notificationTypeEnum == null){
            throw new NotificationTypeException(String.format(
                    "Unable to find service corresponding to notification type: %s", notificationTypeEnum.toString()));
        }
        switch (notificationTypeEnum) {
            case SMS: {
                service = smsService;
                break;
            }
            default:{
                logger.debug("Notification type ",notificationTypeEnum.toString());
                throw new NotificationTypeException(String.format(
                        "Unable to find service corresponding to notification type: %s", notificationTypeEnum.toString()));
            }

        }
        return service;
    }
}
