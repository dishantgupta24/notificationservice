package com.cars24.notificationservice.services;

import com.cars24.notificationservice.dto.NotifyRequestPayload;
import com.cars24.notificationservice.dto.SmsServicePayload;
import com.cars24.notificationservice.exception.NotificationValidationException;
import com.cars24.notificationservice.exception.SmsExecutorException;
import com.cars24.notificationservice.exception.TemplateNotApproved;
import com.cars24.notificationservice.exception.TemplateNotFound;


public interface NotificationService<T> {

    SmsServicePayload transform(NotifyRequestPayload notifyRequestPayload) throws TemplateNotFound, NotificationValidationException, TemplateNotApproved;
    SmsServicePayload validate(T t) throws NotificationValidationException;
    void send(T t) throws NotificationValidationException, SmsExecutorException;
    void process(NotifyRequestPayload notifyRequestPayload) throws TemplateNotFound, TemplateNotApproved;
}
