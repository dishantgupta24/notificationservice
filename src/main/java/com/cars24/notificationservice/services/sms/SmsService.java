package com.cars24.notificationservice.services.sms;

import com.cars24.notificationservice.Utils;
import com.cars24.notificationservice.dto.NotifyRequestPayload;
import com.cars24.notificationservice.dto.SmsServicePayload;
import com.cars24.notificationservice.exception.*;
import com.cars24.notificationservice.logging.LogMessage;
import com.cars24.notificationservice.services.NotificationService;
import com.cars24.notificationservice.services.template.TemplateRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SmsService implements NotificationService<SmsServicePayload> {

    private static Logger logger = LoggerFactory.getLogger(SmsService.class);

    @Autowired
    SmsExecutor smsExecutor;

    @Autowired
    TemplateRenderer templateRenderer;

    @Override
    public SmsServicePayload transform(NotifyRequestPayload notifyRequestPayload) throws TemplateNotFound, TemplateNotApproved {
        String message = templateRenderer.render(
                notifyRequestPayload.getTemplateName(), notifyRequestPayload.getTemplateVariables());

        SmsServicePayload smsServicePayload = new SmsServicePayload();
        smsServicePayload.setMessage(message);
        smsServicePayload.setMobileNumbers(notifyRequestPayload.getMobileNumbers());
        smsServicePayload.setRequestId(notifyRequestPayload.getRequestId());

        return smsServicePayload;
    }

    @Override
    public SmsServicePayload validate(SmsServicePayload smsServicePayload) throws NotificationValidationException {
        List<String> mobileNumbers = smsServicePayload.getMobileNumbers();
        try {
            Utils.isNotEmptyString(smsServicePayload.getMessage());
            mobileNumbers = mobileNumbers.stream().filter(mobileNumber -> {
                try {
                    Utils.isValidMobileNumber(mobileNumber);
                    return true;
                } catch (ValidationException e) {
                    logger.info(LogMessage.getSMSLogMessage(smsServicePayload, e));
                    return false;
                }
            }).collect(Collectors.toList());
            if (mobileNumbers.size() <= 0){
                throw new NotificationValidationException("No valid mobile numbers found.");
            }
        } catch (ValidationException exception) {
            throw new NotificationValidationException(exception.getMessage());
        }
        smsServicePayload.setMobileNumbers(mobileNumbers);
        return smsServicePayload;
    }

    @Override
    public void send(SmsServicePayload smsServicePayload) throws SmsExecutorException {
        smsExecutor.send(smsServicePayload.getMobileNumbers(), smsServicePayload.getMessage());
    }

    @Override
    public void process(NotifyRequestPayload notifyRequestPayload) throws TemplateNotFound, TemplateNotApproved {
        SmsServicePayload smsServicePayload = transform(notifyRequestPayload);
        try {
            smsServicePayload = validate(smsServicePayload);
            send(smsServicePayload);
            logger.info(LogMessage.getSMSLogMessage(smsServicePayload, "OK"));
        } catch (NotificationValidationException | SmsExecutorException e) {
            logger.error(LogMessage.getSMSLogMessage(smsServicePayload, e));
        }
    }
}
