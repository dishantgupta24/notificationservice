package com.cars24.notificationservice.services.sms;


import com.cars24.notificationservice.dto.NotifyRequestPayload;
import com.cars24.notificationservice.exception.NotificationTypeException;
import com.cars24.notificationservice.exception.TemplateNotApproved;
import com.cars24.notificationservice.exception.TemplateNotFound;
import com.cars24.notificationservice.logging.LogMessage;
import com.cars24.notificationservice.services.NotificationService;
import com.cars24.notificationservice.services.NotificationServiceProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.UUID;

@Service
public class SmsQueueListener {

    private static Logger logger = LoggerFactory.getLogger(SmsQueueListener.class);

    @Autowired
    NotificationServiceProvider notificationServiceProvider;

    private ObjectMapper objectMapper = new ObjectMapper();

    @RabbitListener(queues="${jsa.rabbitmq.queue}")
    public void receivedMessage(Message msg) {

        NotifyRequestPayload notifyRequestPayload = null;
        NotificationService notificationService;

        try {
            // parse the message to POJO
            notifyRequestPayload = objectMapper.readValue(msg.getBody(),NotifyRequestPayload.class);
            notifyRequestPayload.setRequestId(UUID.randomUUID().toString());
            logger.info(LogMessage.getRequestLogMessage(notifyRequestPayload, "Request Received"));

            // execute service
            notificationService = notificationServiceProvider.get(
                    notifyRequestPayload.getNotificationTypeEnum());
            notificationService.process(notifyRequestPayload);

        }catch (NotificationTypeException | TemplateNotApproved | TemplateNotFound | IOException e){

            if(e instanceof IOException){
                logger.error(String.format("Unable to parse message: %s, error is: %s", msg, e.getMessage()));
            }else {
                logger.error(LogMessage.getRequestLogMessage(notifyRequestPayload, e));
            }
        }

    }

}
