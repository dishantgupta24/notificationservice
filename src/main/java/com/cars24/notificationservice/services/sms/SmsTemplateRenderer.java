package com.cars24.notificationservice.services.sms;

import com.cars24.notificationservice.entitiy.SmsTemplateEntity;
import com.cars24.notificationservice.exception.TemplateNotApproved;
import com.cars24.notificationservice.exception.TemplateNotFound;
import com.cars24.notificationservice.services.template.TemplateRenderer;
import org.apache.commons.text.StringSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SmsTemplateRenderer implements TemplateRenderer {

    private static Logger logger = LoggerFactory.getLogger(SmsTemplateRenderer.class);

    @Autowired
    SmsTemplateProvider smsTemplateProvider;

    @Override
    public String render(String templateName, Map<String, Object> templateAttributes) throws TemplateNotFound ,TemplateNotApproved{
        SmsTemplateEntity smsTemplateEntity = smsTemplateProvider.get(templateName);
        if(smsTemplateEntity.IsApproved()) {
            StringSubstitutor sub = new StringSubstitutor(templateAttributes);
            String result = sub.replace(smsTemplateEntity.getTemplateContent());
            return result;
        }else {
            logger.debug(String.format("Template not approved %s",templateName));
            throw new TemplateNotApproved("Template not approved");
        }

    }
}
