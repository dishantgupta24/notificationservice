package com.cars24.notificationservice.services.sms;

import com.cars24.notificationservice.exception.GupShupClientMessageEncodingException;
import com.cars24.notificationservice.logging.LogMessage;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class GupShupClient {

    private static Logger logger = LoggerFactory.getLogger(GupShupClient.class);

    private WebClient webClient;

    @Value("${gupshup.baseUrl}")
    private String gupSupBaseUrl;

    @Value("${gupshup.uriPath}")
    private String uriPath;

    @Value("${gupshup.userId}")
    private String userId;

    @Value("${gupshup.password}")
    private String password;

    @PostConstruct
    public void init() {
        this.webClient = WebClient.builder().baseUrl(gupSupBaseUrl).build();
    }

    public void send(List<String> mobileNumbers, String mesaage) throws GupShupClientMessageEncodingException {
        String mobileNumberParam;
        try {
            mesaage = URLEncoder.encode(mesaage,"UTF-8");
            mobileNumberParam = URLEncoder.encode(
                    StringUtils.join(mobileNumbers, ","), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new GupShupClientMessageEncodingException("Failed to encode message.");
        }
        Map<String, String> queryParams = getQueryParams(mobileNumberParam, mesaage);
        callGupShupApi(queryParams);
    }

    private Map<String,String> getQueryParams(String mobileNumber, String message){
        Map<String,String> queryParams = new HashMap<>();
        queryParams.put("method","sendMessage");
        queryParams.put("msg_type","TEXT");
        queryParams.put("userid",userId);
        queryParams.put("password",password);
        queryParams.put("auth_scheme","plain");
        queryParams.put("send_to",mobileNumber);
        queryParams.put("msg",message);
        return queryParams;
    }

    private void callGupShupApi(Map<String,String> queryParams){
        webClient.get().uri(
                uriBuilder -> {
                    queryParams.forEach(uriBuilder.path(uriPath)::queryParam);
                    return uriBuilder.build();
                }
        ).exchange().flatMap(
                res -> res.bodyToMono(String.class)
        ).subscribe(res ->{
            logger.info(LogMessage.getGupShupApiResponseMessage(queryParams.get("send_to"),res));
        })
        ;
    }
}
