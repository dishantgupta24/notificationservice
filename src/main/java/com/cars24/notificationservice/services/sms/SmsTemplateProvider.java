package com.cars24.notificationservice.services.sms;

import com.cars24.notificationservice.entitiy.SmsTemplateEntity;
import com.cars24.notificationservice.exception.TemplateNotFound;
import com.cars24.notificationservice.services.template.SmsTemplateRepository;
import com.cars24.notificationservice.services.template.TemplateProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("sms")
public class SmsTemplateProvider implements TemplateProvider<SmsTemplateEntity> {

    @Autowired
    SmsTemplateRepository smsTemplateRepository;

    @Override
    public SmsTemplateEntity get(String templateName) throws TemplateNotFound {
        try {
            SmsTemplateEntity smsTemplateEntity = smsTemplateRepository.findByTemplateName(templateName);
            if (smsTemplateEntity != null) {
                return smsTemplateEntity;
            } else {
                throw new TemplateNotFound(String.format("Unable to fetch template for name %s.", templateName));
            }
        } catch (Exception exception) {
            throw new TemplateNotFound(String.format("Unable to fetch template for name %s, error: %s",
                    templateName, exception.getMessage()));
        }
    }

    @Override
    public void post(SmsTemplateEntity smsTemplateEntity) {
        smsTemplateRepository.save(smsTemplateEntity);
    }
}
