package com.cars24.notificationservice.services.sms;

import com.cars24.notificationservice.exception.GupShupClientMessageEncodingException;
import com.cars24.notificationservice.exception.SmsExecutorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SmsExecutor {

    @Autowired
    private GupShupClient gupShupClient;

    public void send(List<String> mobileNumbers, String message) throws SmsExecutorException {
        try {
            gupShupClient.send(mobileNumbers,message);
        } catch (GupShupClientMessageEncodingException exception) {
            throw new SmsExecutorException(exception.getMessage());
        }
    }
}
