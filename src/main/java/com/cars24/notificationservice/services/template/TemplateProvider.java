package com.cars24.notificationservice.services.template;

import com.cars24.notificationservice.entitiy.SmsTemplateEntity;
import com.cars24.notificationservice.exception.TemplateNotFound;
import org.springframework.stereotype.Service;

@Service
public interface TemplateProvider<T> {

    SmsTemplateEntity get(String templateName) throws TemplateNotFound;
    void post(T t);
}
