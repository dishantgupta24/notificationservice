package com.cars24.notificationservice.services.template;

import com.cars24.notificationservice.exception.TemplateNotApproved;
import com.cars24.notificationservice.exception.TemplateNotFound;

import java.util.Map;

public interface TemplateRenderer {

    String render(String templateName, Map<String, Object> templateAttributes) throws TemplateNotFound, TemplateNotApproved;
}
