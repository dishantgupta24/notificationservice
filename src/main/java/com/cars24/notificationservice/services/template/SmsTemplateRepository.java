package com.cars24.notificationservice.services.template;

import com.cars24.notificationservice.entitiy.SmsTemplateEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsTemplateRepository extends CrudRepository<SmsTemplateEntity, Integer> {

    SmsTemplateEntity findByTemplateName(String templateName);
}
