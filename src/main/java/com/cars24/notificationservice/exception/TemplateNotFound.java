package com.cars24.notificationservice.exception;

public class TemplateNotFound extends Exception {
    public TemplateNotFound(String s) {
        super(s);
    }
}
