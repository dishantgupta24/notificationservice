package com.cars24.notificationservice.exception;

public class TemplateNotApproved extends Exception {
    public TemplateNotApproved(String s) {
        super(s);
    }

}
