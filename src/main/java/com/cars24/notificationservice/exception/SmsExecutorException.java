package com.cars24.notificationservice.exception;

public class SmsExecutorException extends Exception{
    public SmsExecutorException(String s){
        super(s);
    }
}
