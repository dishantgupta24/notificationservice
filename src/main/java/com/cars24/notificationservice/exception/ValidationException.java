package com.cars24.notificationservice.exception;

public class ValidationException extends Exception{
    public ValidationException(String s) {
        super(s);
    }
}
