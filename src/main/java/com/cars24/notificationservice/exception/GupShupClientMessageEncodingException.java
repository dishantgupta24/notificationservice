package com.cars24.notificationservice.exception;

public class GupShupClientMessageEncodingException extends Exception {
    public GupShupClientMessageEncodingException(String s) {
        super(s);
    }
}
