package com.cars24.notificationservice.exception;

public class NotificationValidationException extends Exception {
    public NotificationValidationException(String s) {
        super(s);
    }
}
