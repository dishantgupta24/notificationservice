package com.cars24.notificationservice.constant;




public class ApiConstant {

    public final  static int SUCCESS_CODE = 200;
    public final static int BAD_RESPONSE_CODE = 400;
    public final static String SUCCESS_MESSAGE = "OK";
}
