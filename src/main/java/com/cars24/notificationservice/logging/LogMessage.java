package com.cars24.notificationservice.logging;

import com.cars24.notificationservice.dto.NotifyRequestPayload;
import com.cars24.notificationservice.dto.SmsServicePayload;

public class LogMessage {

    public static String getSMSLogMessage(SmsServicePayload smsServicePayload, Exception exception){
        return String.format("ERROR %s-%s", smsServicePayload.toString(), exception.getMessage());
    }

    public static String getSMSLogMessage(SmsServicePayload smsServicePayload, String message){
        return String.format("%s-%s", smsServicePayload.toString(), message);
    }

    public static String getGupShupApiResponseMessage(String request, String response){
        return String.format("Gup shup api Response -> %s -> %s",request,response);
    }

    public static String getRequestLogMessage(NotifyRequestPayload notifyRequestPayload, String message) {
        return notifyRequestPayload.toString();
    }

    public static String getRequestLogMessage(NotifyRequestPayload notifyRequestPayload, Exception exception) {
        return String.format("ERROR %s-%s", notifyRequestPayload.toString(), exception.getMessage());
    }
}
