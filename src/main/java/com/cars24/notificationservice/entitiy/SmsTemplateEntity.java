package com.cars24.notificationservice.entitiy;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sms_templates")
public class SmsTemplateEntity implements Serializable {

    @Id
    @Column(name = "id", nullable = false, columnDefinition = "int(11)")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    @Column(name = "template_name", nullable = false, columnDefinition = "varchar(255)", unique = true)
    private String templateName;

    @Column(name = "template_content", nullable = false, columnDefinition = "TEXT")
    private String templateContent;

    @Column(name = "is_approved",nullable = false,columnDefinition = "boolean")
    private Boolean isApproved;

    public Boolean IsApproved() {
        return isApproved;
    }

    public void setApproved(Boolean approved) {
        isApproved = approved;
    }

    @Column(name = "created_date",nullable = false,columnDefinition = "varchar(255)")
    private String createdDate;

    @Column(name = "updated_date",nullable = false,columnDefinition = "varchar(255)")
    private String updatedDate;

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateContent() {
        return templateContent;
    }

    public void setTemplateContent(String templateContent) {
        this.templateContent = templateContent;
    }
}
