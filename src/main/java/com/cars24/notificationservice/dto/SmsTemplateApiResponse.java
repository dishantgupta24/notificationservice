package com.cars24.notificationservice.dto;

import com.cars24.notificationservice.entitiy.SmsTemplateEntity;

import java.util.List;

public class SmsTemplateApiResponse extends ApiResponse {

    private List<SmsTemplateEntity> data;

    private int size;

    public SmsTemplateApiResponse(int status, String message,List<SmsTemplateEntity> smsTemplateEntities) {
        super(status, message);
        this.data = smsTemplateEntities;
    }

    public List<SmsTemplateEntity> getData() {
        return data;
    }

    public void setData(List<SmsTemplateEntity> data) {
        this.data = data;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
