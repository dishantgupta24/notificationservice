package com.cars24.notificationservice.dto;

import java.util.List;

public class SmsServicePayload {

    private String requestId;
    private List<String> mobileNumbers;
    private String message;

    public List<String> getMobileNumbers() {
        return mobileNumbers;
    }

    public void setMobileNumbers(List<String> mobileNumbers) {
        this.mobileNumbers = mobileNumbers;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String toString(){
        return String.format("%s-%s-%s", requestId, mobileNumbers, message);
    }
}
