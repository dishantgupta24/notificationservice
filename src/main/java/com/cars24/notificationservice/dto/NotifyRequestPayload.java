package com.cars24.notificationservice.dto;

import com.cars24.notificationservice.constant.NotificationTypeEnum;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;


public class NotifyRequestPayload {

    @NotNull(message = "Notification Type is mandatory")
    private NotificationTypeEnum notificationTypeEnum;

    private String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    private List<String> emailList;

    private List<String>mobileNumbers;

    private List<String> pushTokens;

    @NotNull(message = "Template Name is mandatory")
    private String templateName;

    @NotNull(message = "Template Variables is mandatory")
    private Map<String,Object> templateVariables;

    public NotificationTypeEnum getNotificationTypeEnum() {
        return notificationTypeEnum;
    }

    public void setNotificationTypeEnum(NotificationTypeEnum notificationTypeEnum) {
        this.notificationTypeEnum = notificationTypeEnum;
    }

    public List<String> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<String> emailList) {
        this.emailList = emailList;
    }

    public List<String> getMobileNumbers() {
        return mobileNumbers;
    }

    public void setMobileNumbers(List<String> mobileNumbers) {
        this.mobileNumbers = mobileNumbers;
    }

    public List<String> getPushTokens() {
        return pushTokens;
    }

    public void setPushTokens(List<String> pushTokens) {
        this.pushTokens = pushTokens;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public Map<String, Object> getTemplateVariables() {
        return templateVariables;
    }

    public void setTemplateVariables(Map<String, Object> templateVariables) {
        this.templateVariables = templateVariables;
    }

    public String toString(){
        if(notificationTypeEnum.equals(NotificationTypeEnum.SMS)) {
            return String.format("%s-%s-%s-%s",requestId, mobileNumbers, templateName, templateVariables.toString());
        }
        return String.format("%s-%s-%s-%s",requestId,notificationTypeEnum,templateName, templateVariables.toString());
    }
}
